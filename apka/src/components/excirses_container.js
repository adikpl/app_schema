import React, { Component } from 'react'
import axios from 'axios'
import './Table.css'
import TimeSelector from './time_selector'
import Moment from 'moment'

class ExcirsesContainer extends Component {
  render() {
    Moment.locale('en')
  return (
    <div>
      <div className="tile" >
        <TimeSelector />
        <table className="tabelka">
          <tbody>
            <tr className="heads">
              <th>keyword</th>
              <th>best ranking</th>
              <th>date of occurrence</th>
              <th>worst ranking</th>
              <th>date of occurrence</th>
              <th>mean ranking</th>
              <th>groups</th>
            </tr>
            {this.state.excirses.map((excirse) => {
              return(
            <tr className="bod" key={excirse.id} >
              <td>{excirse.keyword}</td>
              <td>{parseFloat(excirse.best_ranking).toFixed(0)}</td>
              <td>{Moment(excirse.date_of_occurrence).format('MMM DD, YYYY')}
              </td>
              <td>{parseFloat(excirse.worst_ranking).toFixed(0)}</td>
              <td>{Moment(excirse.second_date).format('MMM DD, YYYY')}</td>
              <td>{excirse.mean_ranking}</td>
              <td>{excirse.groups}</td>
            </tr>
          )
          })}
          </tbody>
        </table>
      </div>

    </div>
  );
}

  constructor(props) {
    super(props)
    this.state = {
      excirses: []
    }
  }

  componentDidMount() {
    axios.get('http://localhost:3001/excirses.json')
    .then(response => {
      console.log(response)
      this.setState({excirses: response.data})
    })
    .catch(error => console.log(error))
  }


}

export default ExcirsesContainer
