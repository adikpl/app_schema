import React, { Component } from 'react'
import axios from 'axios'
import './Header.css'

class TimeSelector extends Component {

  constructor(props) {
     super(props);
     this.state = {
       weekfilters: []
     }

     this.handleChange = this.handleChange.bind(this);
   }

   handleChange(event) {
     this.setState({value: event.target.value});
   }

   handleSubmit(event) {
     event.preventDefault();
   }

   componentDidMount() {
     axios.get('http://localhost:3001/weekfilters.json')
     .then(response => {
       console.log(response)
       this.setState({weekfilters: response.data})
     })
     .catch(error => console.log(error))
   }

   render() {
     return (
       <form className="head" onSubmit={this.handleSubmit}>
         <label>
           View keyword statistics for 
               <select className="selector" value={this.state} onChange={this.handleChange}>
                {this.state.weekfilters.map((weekfilter) => {
                 return(
                 <option key={weekfilter.id} >{weekfilter.label}</option>
               )}
             )
           }
           </select>
         </label>
       </form>
     )}
}


export default TimeSelector
