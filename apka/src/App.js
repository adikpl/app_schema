import React, { Component } from 'react'
import './App.css'
import ExcirsesContainer from './components/excirses_container'

class App extends Component {
  render() {
    return (
      <div className="App">
        <ExcirsesContainer />
      </div>
    );
  }
}

export default App
