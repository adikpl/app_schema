excirses = Excirse.create([
  {
    keyword: 'prognoza pogody legionowo',
    best_ranking: 11 ,
    date_of_occurrence: Date.new(2017, 8, 9),
    worst_ranking: 23,
    second_date: Date.new(2017, 8, 27),
    mean_ranking: 21.89,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'pogoda wrocław',
    best_ranking: 17,
    date_of_occurrence: Date.new(2017, 8, 11),
    worst_ranking: 20,
    second_date: Date.new(2017, 8, 28),
    mean_ranking: 20.19,
    groups: 'tmp othergroup test all'
  },
  {
    keyword: 'pogoda łódź',
    best_ranking: 6,
    date_of_occurrence: Date.new(2017, 8, 25),
    worst_ranking: 15,
    second_date: Date.new(2017, 8, 8),
    mean_ranking: 12.42,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'prognoza pogody zakopane',
    best_ranking: 18,
    date_of_occurrence: Date.new(2017, 8, 14),
    worst_ranking: 22,
    second_date: Date.new(2017, 8, 19),
    mean_ranking: 22.58,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'pogoda poznań',
    best_ranking: 77,
    date_of_occurrence: Date.new(2017, 8, 16),
    worst_ranking: 89,
    second_date: Date.new(2017, 8, 19),
    mean_ranking: 89.42,
    groups: 'tmp othergroup test all'
  },
  {
    keyword: 'pogoda szczyrk',
    best_ranking: 95,
    date_of_occurrence: Date.new(2017, 8, 15),
    worst_ranking: 99,
    second_date: Date.new(2017, 9, 5),
    mean_ranking: 96.19,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'pogoda katowice',
    best_ranking: 32,
    date_of_occurrence: Date.new(2017, 9, 5),
    worst_ranking: 42,
    second_date: Date.new(2017, 8, 13),
    mean_ranking: 38.09,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'pogoda na mazowszu',
    best_ranking: 72,
    date_of_occurrence: Date.new(2017, 8, 29),
    worst_ranking: 79,
    second_date: Date.new(2017, 8, 24),
    mean_ranking: 75.39,
    groups: 'tmp othergroup test all'
  },
  {
    keyword: 'prognoza pogody kraków',
    best_ranking: 80,
    date_of_occurrence: Date.new(2017, 8, 22),
    worst_ranking: 97,
    second_date: Date.new(2017, 8, 13),
    mean_ranking: 83.01,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'pogoda nad morzem',
    best_ranking: 22,
    date_of_occurrence: Date.new(2017, 8, 16),
    worst_ranking: 31,
    second_date: Date.new(2017, 8, 8),
    mean_ranking: 23.56,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'pogoda sopot',
    best_ranking: 30,
    date_of_occurrence: Date.new(2017, 8, 13),
    worst_ranking: 46,
    second_date: Date.new(2017, 8, 31),
    mean_ranking: 37.75,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'prognoza pogody białystok',
    best_ranking: 51,
    date_of_occurrence: Date.new(2017, 8, 11),
    worst_ranking: 67,
    second_date: Date.new(2017, 9, 4),
    mean_ranking: 58.04,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'pogoda warszawa',
    best_ranking: 55,
    date_of_occurrence: Date.new(2017, 8, 9),
    worst_ranking: 71,
    second_date: Date.new(2017, 8, 14),
    mean_ranking: 61.7,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'pogoda sopot',
    best_ranking: 3,
    date_of_occurrence: Date.new(2017, 8, 19),
    worst_ranking: 21,
    second_date: Date.new(2017, 8, 17),
    mean_ranking: 11.81,
    groups: 'tmp othergroup test all'
  },
  {
    keyword: 'prognoza pogody sopot',
    best_ranking: 18,
    date_of_occurrence: Date.new(2017, 8, 19),
    worst_ranking: 30,
    second_date: Date.new(2017, 9, 3),
    mean_ranking: 23.02,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'prognoza pogody poznań',
    best_ranking: 83,
    date_of_occurrence: Date.new(2017, 8, 24),
    worst_ranking: 98,
    second_date: Date.new(2017, 9, 1),
    mean_ranking: 95.44,
    groups: 'tmp othergroup all'
  },
  {
    keyword: 'prognoza pogody rzeszów',
    best_ranking: 4,
    date_of_occurrence: Date.new(2017, 8,11),
    worst_ranking: 7,
    second_date: Date.new(2017, 8,13),
    mean_ranking: 5.91,
    groups: 'tmp othergroup all'
  }
])
weekfilters = WeekFilter.create([
  {
    label: '1 week'
  },
  {
    label: '2 weeks'
  },
  {
    label: '1 month'
  }
])
