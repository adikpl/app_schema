class CreateExcirses < ActiveRecord::Migration[5.1]
  def change
    create_table :excirses do |t|
      t.string :keyword
      t.decimal :best_ranking
      t.date :date_of_occurrence
      t.decimal :worst_ranking
      t.date :second_date
      t.float :mean_ranking
      t.string :groups
      t.timestamps
    end
  end
end
