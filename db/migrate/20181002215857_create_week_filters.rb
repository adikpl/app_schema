class CreateWeekFilters < ActiveRecord::Migration[5.1]
  def change
    create_table :week_filters do |t|
      t.string :label
      t.timestamps
    end
  end
end
