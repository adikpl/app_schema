class WeekfiltersController < ApplicationController
  def index
    @weekfilters = WeekFilter.all
    render json: @weekfilters
  end
end
