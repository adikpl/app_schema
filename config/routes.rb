Rails.application.routes.draw do
  resources :items
  scope 'excirses' do
    resources :excirses, as:'excirses'
  end
  resources :excirses

  scope 'week_filters' do
    resources :index, as: 'weekfilters'
  end
  resources :weekfilters


  post '/authenticate'=> 'authentication#authenticate', :as => :authentication
end
